#pragma once
class Card
{
public:
	enum class Number {NONE,one=1,two,three};
	enum class Symbol {NONE, diamond, squiggle, oval};
	enum class Shading { NONE, solid, striped, open };
	enum class Color { NONE, red, green, blue };
private:
	Number m_number;
	Symbol m_symbol;
	Shading m_shading;
	Color m_color;

public:
	Card();
	Card(const Number& n, const Symbol& s, const Shading& sh, const Color& c);
	Card(const Card& card);
	Card& operator = (const Card& card);
	~Card();
	void printCard() const;
};

