#include "Deck.h"
#include <iostream>
#include <random>


Deck::~Deck()
{
}

void Deck::printDeck() const
{
	for (auto&& elem : m_deck)
	{
		elem.printCard();
		std::cout << std::endl;
	}
}


void swap(Card &firstCard, Card &secondCard)
{
	Card temp = firstCard;
	firstCard = secondCard;
	secondCard = temp;
}

void Deck::shuffleDeck()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, 81);

	for (auto&& elem : m_deck)
	{
		int randomCardIndex = dis(gen);
		swap(elem, m_deck.at(randomCardIndex));
	}
}