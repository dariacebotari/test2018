#include "Card.h"
#include <iostream>

Card::Card() :m_number(Number::NONE), m_symbol(Symbol::NONE), m_shading(Shading::NONE), m_color(Color::NONE)
{
}

Card::Card(const Number & n, const Symbol & s, const Shading & sh, const Color & c): m_number(n),m_symbol(s),m_shading(sh),m_color(c)
{
}


Card::Card(const Card& card)
{
	*this = card;
}

Card& Card::operator=(const Card& card)
{
	m_number = card.m_number;
	m_symbol = card.m_symbol;
	m_shading = card.m_shading;
	m_color = card.m_color;
	return *this;
}

void Card::printCard() const
{
	switch (m_number)
	{
	case Number::one:
		std::cout << "1";
		break;
	case Number::two:
		std::cout << "2";
		break;
	case Number::three:
		std::cout << "3";
		break;
	default:
		std::cout << static_cast<std::underlying_type<Number>::type>(m_number);
		break;
	}
	std::cout << " ";
	switch (m_symbol)
	{
	case Symbol::diamond:
		std::cout << "diamond";
		break;
	case Symbol::squiggle:
		std::cout << "squiggle";
		break;
	case Symbol::oval:
		std::cout << "oval";
		break;
	default:
		break;
	}
	std::cout << " ";
	switch (m_shading)
	{
	case Shading::solid:
		std::cout << "solid";
		break;
	case Shading::striped:
		std::cout << "striped";
		break;
	case Shading::open:
		std::cout << "open";
		break;
	default:
		break;
	}
	std::cout << " ";
	switch (m_color)
	{
	case Color::red:
		std::cout << "red";
		break;
	case Color::green:
		std::cout << "green";
		break;
	case Color::blue:
		std::cout << "blue";
		break;
	default:
		break;
	}
}

Card::~Card()
{
}