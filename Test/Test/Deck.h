#pragma once
#include "Card.h"
#include <iostream>
#include <array>
#include "Game.h"

class Deck
{
	std::array<Card, 81> m_deck;
	int m_cardIndex = 0;
public:
	Deck();
	~Deck();
	void printDeck() const;
	void shuffleDeck();
};


