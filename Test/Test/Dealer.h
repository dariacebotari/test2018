#pragma once
#include "Deck.h"
#include<vector>

class Dealer : public Game
{
public:
	Dealer(const std::string& name, Deck* deck);
	Dealer(const Dealer& other);
	~Dealer() = default;
	Dealer& operator=(const Dealer& other);
	std::vector<Card> dealCards(const size_t& count);
private:
	Deck * deck;
};
